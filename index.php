<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Casagest24 | Accesso agenzie immobiliari</title>
        <meta name="description" content="Il Gestionale per Agenzie Immobiliari che ti consente di lavorare meno, lavorare meglio e avere più risultati">
        <meta name="author" content="Insieme Srl">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,300,100,500,700,900" rel="stylesheet">
        <link href="media/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="media/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="media/css/core.css" rel="stylesheet" type="text/css">
        <link href="media/css/components.css" rel="stylesheet" type="text/css">
        <link href="media/css/colors.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="media/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="media/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="media/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="media/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="media/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="media/js/core/app.js"></script>
        <script>
            $(function () {
                $('.styled').uniform();
                $(".recupera_password").click(function () {
                    $('#box_recupera_agenzia').toggle();
                });
                $('input[name="tipo_utente"]').change(function () {
                    var val = $(this).val();
                    if (val === 'agenzia') {
                        $('#box_recupera_agenzia').toggle();
                    } else {
                        $('#box_recupera_agenzia').fadeOut();
                    }
                });
                $(".recupera_password_cliente").click(function () {
                    $('#box_recupera_agenzia').fadeOut();
                });
            });
        </script>
        <script type="text/javascript">
            var _iub = _iub || [];
            _iub.csConfiguration = {"lang": "it", "siteId": 1298617, "cookiePolicyId": 18625523};
        </script>
        <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PPRP847');</script>
        <!-- End Google Tag Manager -->
    </head>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPRP847" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <body class="login-container">
        <div class="page-container sfondo-login">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        <div class="container">
                            <div class="text-center content-group mt-20">
                                <a href="https://casagest24.it/" title="Casagest24">
                                    <img src="media/images/logo.png" alt="Casagest24" class="img-responsive display-inline-block">
                                </a>
                                <h1 class="error-title mt-50 hidden-xs" style="font-size: 30px; text-shadow: none; font-weight: 600;">Ciò che altri stanno ancora pensando noi lo abbiamo già realizzato!</h1>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                                            <div class="panel panel-body padding-bottom-0">
                                                <form action="https://casagest24.it/application/controller/operation.php?evento=login" method="post">
                                                    <div class="text-center form-group">
                                                        <h5 class="text-danger text-bold mt-5">ACCESSO AGENZIE IMMOBILIARI</h5>
                                                    </div>
                                                    <div class="content-divider text-muted form-group"><span>Inserisci le tue credenziali</span></div>
                                                    <input name="tipo_utente" type="hidden" value="1">
                                                    <div class="form-group has-feedback">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-user"></i></span>
                                                            <input class="form-control" name="username" placeholder="Username" type="text" required="required">
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-feedback">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                                            <input class="form-control" name="userpass" placeholder="Password" type="password" required="required">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" name="login" class="btn bg-success-400 btn-block btn-xlg text-bold text-uppercase">Accedi</button>
                                                    </div>
                                                    <div class="form-group login-options">
                                                        <div class="row">
                                                            <div class="col-xs-12 text-right">
                                                                <a class="text-danger recupera_password" href="javascript:void(0);">Password dimenticata?</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div id="box_recupera_agenzia" style="display: none;">
                                                    <div class="content-divider text-muted form-group"><span>Password dimenticata?</span></div>
                                                    <div class="mb-10 text-center">
                                                        <h7 class="text-danger text-bold">Inserisci username per richiedere il cambio password</h7>
                                                    </div>
                                                    <form action="https://casagest24.it/application/controller/operation.php?evento=recuperaPsw" method="post">
                                                        <input  name="tipo" type="hidden" value="agenzia">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                                                <input class="form-control" name="username" placeholder="Username" type="text" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn bg-warning btn-block btn-xlg text-bold text-uppercase">Richiedi password</button>
                                                        </div>
                                                    </form>
                                                    <div class="content-divider text-muted form-group"><span></span></div>
                                                </div>
                                                <div class="content-divider text-muted form-group"><span>Non sei registrato?</span></div>
                                                <a href="mailto:info@casagest24.it?subject=Informazioni%20per%20registrazione%20da%20Casagest24.it" class="btn btn-default btn-block content-group-xs">Contattaci</a>
                                                <div class="text-center mb-10">
                                                    <small class="text-muted">Per una migliore esperienza d'uso, consigliamo <a href="https://www.mozilla.com/it/firefox/" target="_blank" class="text-warning">Firefox</a></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-20 hidden-xs">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="panel panel-body">
                                                <div class="text-center form-group">
                                                    <h4 class="text-black text-bold mt-10 text-uppercase">Perchè Casagest24 è il gestionale numero uno?</h4>
                                                </div>
                                                <div class="row mt-20 text-center">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <button type="button" class="btn btn-primary btn-xlg text-bold text-uppercase mt-10" data-toggle="modal" data-target="#modal_funzioni1">Funzioni esclusive di Casagest24</button>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <button type="button" class="btn btn-primary btn-xlg text-bold text-uppercase mt-10" data-toggle="modal" data-target="#modal_funzioni2">Funzioni che hanno tutti i gestionali</button>
                                                    </div>
                                                </div>
                                                <div class="text-center mt-20">
                                                    <h6 class="text-black text-bold"><u>Aggiornarsi conviene!</u></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_funzioni1" class="modal fade">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title text-primary-800">Funzioni esclusive di Casagest24</h5>
                    </div>
                    <div class="modal-body">
                        <ul class="list list-square no-margin-bottom">
                            <li>Gestione avanzata dei <strong>clienti</strong> che cercano casa</li>
                            <li>Gestione avanzata della lista degli <strong>immobili</strong></li>
                            <li>Gestione avanzata della <strong>collaborazione</strong> (M.L.S.)</li>
                            <li>Gestione avanzata degli <strong>appuntamenti</strong></li>
                            <li>Oltre 10 <strong>applicativi</strong> collegati per gestire a 360° tutte le attività dell'Agenzia Immobiliare</li>
                            <li><strong>Aggiornamenti</strong> ed <strong>implementazioni</strong> senza sosta</li>
                            <li><strong>Formazione</strong> ed <strong>Assistenza</strong> continua</li>
                        </ul>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal_funzioni2" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title text-primary-800">Funzioni che hanno tutti i gestionali</h5>
                    </div>
                    <div class="modal-body">
                        <ul class="list list-square no-margin-bottom">
                            <li>Gestione del portfolio clienti</li>
                            <li>Gestione schede immobili</li>
                            <li>Collaborazione con altri agenti</li>
                            <li>Gestione calendario e agenda</li>
                            <li>Assistenza di basso livello</li>
                        </ul>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Chiudi</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer_copy">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-left">
                    &copy; <?= date("Y") ?> Casagest24 | P.IVA 01899700502 | Marchio e progetto di <a href="https://www.gruppoinsieme.it" target="_blank" title="Gruppoinsieme">Insieme S.r.l.</a>
                </div>
                <div class="col-xs-12 col-sm-6 text-right">
                    <a href="https://www.iubenda.com/privacy-policy/18625523" class="iubenda-nostyle iubenda-noiframe iubenda-embed iubenda-noiframe " title="Privacy Policy ">Privacy Policy</a> | <a href="https://www.iubenda.com/privacy-policy/18625523/cookie-policy" class="iubenda-nostyle iubenda-noiframe iubenda-embed iubenda-noiframe " title="Cookie Policy ">Cookie Policy</a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            (function (w, d) {
                var loader = function () {
                    var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                    s.src = "https://cdn.iubenda.com/iubenda.js";
                    tag.parentNode.insertBefore(s, tag);
                };
                if (w.addEventListener) {
                    w.addEventListener("load", loader, false);
                } else if (w.attachEvent) {
                    w.attachEvent("onload", loader);
                } else {
                    w.onload = loader;
                }
            })(window, document);</script>
    </body>
</html>